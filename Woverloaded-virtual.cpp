#include "Woverloaded-virtual.h"

class Oops: public Foo {
};

class Bar : public Oops {
public:
   void f(int) final {};
};


int main(int argc, char *argv[]) {
  Bar b;
  b.f(1);
  return 0;
}
